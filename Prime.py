import sys
import math

def prime(num):
    while(num % 2 == 0):
        print (2, end=" ")
        num = int(num / 2)
    for i in range(3,int(math.sqrt(num)) + 1,2):
        while (num % i == 0):
            print(i, end=" ")
            num = int(num / i)
    if(num > 2):
        print (num)


if(len(sys.argv) > 1):
    n = int(sys.argv[1])
    prime(n)
else:
    n = int(input("Give me a number: "))
    prime(n)

print("\n")
