
itnums = []
startnum = 1
pnum = 1
def collatz(num):
    if (num % 2 == 0):
        num = num / 2
        itnums.append(num)
    else:
        num = 3 * num + 1
        itnums.append(num)
    if (num != 1):
        collatz(num)
    power()



def power():
    for i in reversed(range(len(itnums) - 1)):
        if(itnums[i] % 2 == 1):
            n = int(i)
            return n


def add_to_file():
    f = open('ColData2p18-2p19.csv', 'a')
    f.write("{}, {}, {}\n".format(startnum, pnum, itnums[pnum]))
    f.close()

for i in range(2**18 + 1, 2**19 + 1):
    startnum = i
    itnums.append(i)
    collatz(int(i))
    pnum = power()
    if(pnum == None):
        pnum = 0
    else:
        pnum += 1
    add_to_file()
    itnums = []


