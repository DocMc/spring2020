#include <iostream>

long Ack(long m,long n){
	if (m == 0){
		return n + 1;
	}
	else if(n == 0){
		return Ack(m - 1, 1);
	}
	else{
		return Ack(m - 1, Ack(m, n - 1));
	}
}

int main(){
	for (long m = 0; m < 6; m++){
		for (long n = 0; n < 5; n++){
			std::cout << m << "," << n << " = " << Ack(m,n) << std::endl;
		}
	}
	return 0;
}
	
