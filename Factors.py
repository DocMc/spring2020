import math
import sys
def factor(num):
    fArray = []
    for i in range(1, int(math.sqrt(num)) + 1):
        if(num % i == 0):
            fArray.append(i)
    for n in reversed(fArray):
        fArray.append(int(num / n))
    print(fArray)

if(len(sys.argv) <= 1):
    n = int(input("Give me a number: "))
    factor(n)
else:
    for n in sys.argv:
        if (n != sys.argv[0]):
            num = int(n)
            factor(num)

