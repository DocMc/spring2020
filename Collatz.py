import sys
from time import sleep
from os import system
sys.setrecursionlimit(300000000)

def collatz(num):
    if (num % 2 == 0):
        num = num / 2
        if(len(sys.argv) > 1):
            print(num),
    else:
        num = 3 * num + 1
        if(len(sys.argv) > 1):
            print(num),
    if (num == 1):
        print("Done"),
    else:
        collatz(num)

if(len(sys.argv) > 1):
    num = int(sys.argv[1])
    collatz(num)
else:
    try:
        num = 1
        while (True):
            system("clear")
            sleep(.01)
            print(num)
            collatz(num)
            num = num + 1
            sleep(.25)
    except:
        print(num)
