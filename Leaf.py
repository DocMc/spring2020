import random
from PIL import Image

class BarnsleyFern(object):
    def __init__(self, img_w, img_h, color=(0,150,0), bColor=(255,255,255)):
        self.img_w, self.img_h = img_w, img_h
        self.color = color
        self.x, self.y = 0, 0
        self.age = 0

        self.fern = Image.new("RGB", (img_w, img_h), bColor)
        self.pix = self.fern.load()
        self.pix[self.scale(0,0)] = color

    def scale(self, x, y):
        h = (x + 2.182)*(self.img_w - 1)/4.8378
        k = (9.9983 - y)*(self.img_h - 1)/9.9983
        return h, k

    def transform(self, x, y):
        rand = random.uniform(0,100)
        if rand < 1:
            return 0, 0.16*y
        elif 1 <= rand < 86:
            return 0.85*x + 0.04*y, -0.04*x + 0.85*y + 1.6
        elif 86 <= rand < 93:
            return 0.2*x - 0.26*y, 0.23*x + 0.22* y + 1.6
        else:
            return -0.15*x + 0.28*y, 0.26* x + 0.24*y + 0.44

    def iterate(self, i):
        for _ in range(i):
            self.x, self.y = self.transform(self.x, self.y)
            self.pix[self.scale(self.x, self.y)] = self.color
        self.age += i

fern = BarnsleyFern (1000,1000)
fern.iterate(5000000)
fern.fern.show()
fern.fern.save("Leaf.jpeg")
