#include <iostream>
#include <string>
#include <algorithm>
#include <regex>
using namespace std;
float Add(float num1, float num2){
	return num1 + num2;
}
float Sub(float num1, float num2){
	return num1 - num2;
}
float Pro(float num1,float  num2){
	return num1 * num2;
}
float Div(float num1, float num2){
	if (num1 == 0){
		cout << "You can't devide by ZERO" << endl;
		return 0;
	}
	else{
		return num1/num2;
	}
}

int main(){
	int mainLoop = 1;
	while (mainLoop == 1){
	float num1;
	cout << "Give me a number "<< endl;
	string numString1;
	getline(cin, numString1);
	float  num2;
	cout << "Give me another number" << endl;
	string numString2;
	getline(cin, numString2);
	num1 = stoi(numString1);
	num2 = stoi(numString2);
	int choice;
	string choiceString;
	float answer;
	int loop = 0;
	while (loop == 0){
	cout << "What would you like to do?" << endl
	<< "1) Add" << endl
	<< "2) Subtract" << endl
	<< "3) Multiply" << endl
	<< "4) Divide" << endl 
	<< "You are going to choose: ";
	getline(cin, choiceString);
	choice = stoi(choiceString);
	switch(choice){
		case 1:
		answer = Add(num1,num2);
		loop = 1;
		break;
		case 2:
		answer = Sub(num1, num2);
		loop = 1;
		break;
		case 3:
		answer = Pro(num1,num2);
		loop = 1;
		break;
		case 4:
		answer = Div(num1,num2);
		loop = 1;
		break;
		default:
		cout << "not a choice" << endl;
		loop = 0;
		break;
	}
	}
	cout << "The answer is " << answer << endl;
	cout << "Would you like to do another? y/n" << endl;
	string yn;
	getline(cin, yn);
	for_each(yn.begin(), yn.end(), [](char & c){
		c = tolower(c);
	});
	if (yn != "y"){
		mainLoop = 0;
	}
	}
	return 0;
}
